import axios from 'axios';

export const fetchBuildingZones = async () => {
  const { data } = 
    await axios.get('https://my-json-server.typicode.com/ivanturianytsia-envio/json-data/zones');

  return data;
};