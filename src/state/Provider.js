import React, { createContext, useReducer } from 'react';
import { UIReducer } from './reducer';

const initialState = {
  temperature: 'C',
  schedules: [],
  buildingFilter: ''
};

const { data: persistedSchedules } = JSON.parse(localStorage.getItem('schedules') || '{"data": []}');
if (persistedSchedules.length > 0) initialState.schedules = persistedSchedules;

export const UIStateProvider = ({ children }) => {
  const [state, dispatch] = useReducer(UIReducer, initialState);

  return (
    <UIStateContext.Provider value={[state, dispatch]}>
      {children}
    </UIStateContext.Provider>
  );
};

export const UIStateContext = createContext(initialState);
