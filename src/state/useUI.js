import { useContext } from 'react';
import { UIStateContext } from './Provider';

export const useUI = () => {
  const [state, dispatch] = useContext(UIStateContext);
  const { temperature, schedules, buildingFilter } = state;

  const setTemperature = (temperature) =>
    dispatch({ type: 'SET_TEMPERATURE', payload: temperature });
  
  const setBuildingFilter = (buildingId) =>
    dispatch({ type: 'SET_BUILDING_FILTER', payload: buildingId });

  const updateSchedule = (schedule) =>
    dispatch({ type: 'UPDATE_SCHEDULE', payload: schedule});

  const removeSchedule = (schedule) =>
    dispatch({ type: 'REMOVE_SCHEDULE', payload: schedule});
  
  const addSchedule = (schedule) =>
    dispatch({type: 'ADD_SCHEDULE', payload: schedule });

  return {
    temperature,
    setTemperature,
    buildingFilter,
    setBuildingFilter,
    schedules,
    updateSchedule,
    addSchedule,
    removeSchedule
  };
}