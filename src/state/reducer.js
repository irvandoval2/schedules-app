import { v4 as uuidv4 } from 'uuid';

export const UIReducer = 
  (state, { type, payload }) => {
    switch (type) {
      case 'SET_TEMPERATURE': {
        return {...state, temperature: payload};
      }
      case 'SET_BUILDING_FILTER': {
        return {...state, buildingFilter: payload};
      }
      case 'REMOVE_SCHEDULE': {
        const schedules = 
          JSON.parse(localStorage.getItem('schedules') || '{"data": []}').data;
        const newSchedules = schedules.filter((item) => item.id !== payload.id);
        localStorage.setItem('schedules', JSON.stringify({data: newSchedules}));
        return {...state, schedules: newSchedules};
      }
      case 'UPDATE_SCHEDULE': {
        const schedules = 
          JSON.parse(localStorage.getItem('schedules') || '{"data": []}').data;
        const newSchedules = schedules.filter((item) => item.id !== payload.id);
        localStorage.setItem('schedules', JSON.stringify({data: [...newSchedules, payload]}));
        return {...state, schedules: [...newSchedules, payload]};
      }
      case 'ADD_SCHEDULE': {
        const schedules = 
          JSON.parse(localStorage.getItem('schedules') || '{"data": []}').data;
        const newSchedules = [...schedules, {...payload, id: uuidv4()}]
        localStorage.setItem('schedules', JSON.stringify({data: newSchedules}));
        return {...state, schedules: [...newSchedules]};
      }
      default:
        return state;
    }
};