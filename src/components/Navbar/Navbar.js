import React from 'react';
import { Form, Navbar as BootstrapNavbar, Button, Container, Row, Col } from 'react-bootstrap';
import Loader from 'react-loader-spinner';
import { useQuery } from 'react-query';
import { useLocation, useHistory } from 'react-router-dom';
import { fetchBuildingZones } from '../../services/buildingzone';
import { useUI } from '../../state/useUI';
import './navbar.css';

const Navbar = () => {
  const location = useLocation();
  const pathname = location.pathname;
  const history = useHistory();
  const { temperature, setTemperature, buildingFilter, setBuildingFilter } = useUI();
  const isFarenheit = temperature === 'F';
  const { isLoading, data: buildingZones} = useQuery('buildings', fetchBuildingZones);
  const onCheckChange = () => {
    const newValue = isFarenheit ? 'C' : 'F';
    setTemperature(newValue);
  };

  const onNewClick = (add) => () => 
    add ? history.push('/schedules/new') : history.push('/schedules');

  return (
    <BootstrapNavbar className='bg-light justify-content-between shadow-sm'>
      <Container fluid>
        <Row className='NavbarRow'>
          <Col lg={4} md={5} sm={8} xs={12} >
            <BootstrapNavbar.Brand>
              {pathname === '/schedules' ? 'Schedules Overview' : 'Schedules Edit'}
            </BootstrapNavbar.Brand>
            {
              pathname === '/schedules' ? (
                <Button
                  variant='success'
                  className='HeaderButton'
                  size='sm'
                  onClick={onNewClick(true)}
                >
                  + New
                </Button>
              ) : (
                <Button
                  variant='primary'
                  size='sm'
                  className='HeaderButton'
                  onClick={onNewClick(false)}
                >
                  &#60; Go Back
                </Button>
              )
            }
          </Col>
          {pathname === '/schedules' && (<Col lg={5} md={5} sm={8} xs={12} >
            {
              isLoading ?
                (<Loader
                  type='ThreeDots'
                  color='#00BFFF'
                  height={100}
                  width={100}
                  timeout={3000}
                />) :
                (<Form inline>
                  <Form.Label id='FilterLabel'>Filter By Building Zone:</Form.Label>
                  <Form.Control
                    type='text'
                    as='select'
                    value={buildingFilter}
                    onChange={({ target: { value } }) => setBuildingFilter(value)}
                  >
                    <option value=''>Show All</option>
                    {buildingZones.map(({ id, name }) => (<option key={id} value={id}>{name}</option>))}
                  </Form.Control>
                </Form>)
            }
          </Col>)}
          <Col>
            <Form inline id='NavbarForm'>
              <Form.Label id='TemperatureCelsius' className={!isFarenheit ? 'TempSelected' : ''}>°C</Form.Label>
              <Form.Check
                type='switch'
                id='TemperatureSwitch'
                defaultChecked={isFarenheit}
                onChange={onCheckChange}
              />
              <Form.Label className={isFarenheit ? 'TempSelected' : ''}>°F</Form.Label>
            </Form>
          </Col>
        </Row>
      </Container>
    </BootstrapNavbar>
  )
};

export default Navbar;