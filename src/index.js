import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { UIStateProvider } from './state/Provider';
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom';
import reportWebVitals from './reportWebVitals';
import { Schedules } from './pages/Schedules';
import { EditSchedules } from './pages/EditSchedules';
import { QueryClient, QueryClientProvider } from 'react-query';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Navbar } from './components/Navbar';


const queryClient = new QueryClient()

ReactDOM.render(
  <QueryClientProvider client={queryClient}>
      <UIStateProvider>
      <Router>
      <Navbar />
        <Switch>
          <Route exact path='/schedules' component={Schedules} />
          <Route exact path='/schedules/:id'>
            <EditSchedules />
          </Route>
          <Route render={() => <Redirect to='/schedules' />} />
        </Switch>
    </Router>
    </UIStateProvider>
  </QueryClientProvider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
