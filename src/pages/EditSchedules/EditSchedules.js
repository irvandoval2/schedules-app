import React, { useEffect, useMemo, useState } from 'react';
import Loader from 'react-loader-spinner';
import TimePicker from 'react-bootstrap-time-picker';
import { useQuery } from 'react-query';
import { useUI } from '../../state/useUI';
import { useParams, useHistory } from 'react-router-dom';
import { fetchBuildingZones } from '../../services/buildingzone';
import DropdownMultiselect from 'react-multiselect-dropdown-bootstrap';
import { Container, Form, Button, Row, Col, Alert, ListGroup, Badge, Modal } from 'react-bootstrap';
import { CtoF, FtoC, secondsToAMPM } from '../../utils';

import './edit-schedules.css';

const defaultForm = {
  startTime: 0,
  endTime: 1800,
  title: '',
  temperature: '0'
};

const EditSchedules = () => {
  const history = useHistory();
  const {
    temperature,
    schedules,
    addSchedule,
    updateSchedule,
    removeSchedule
  } = useUI();
  const [isSubmitted, setIsSubmitted] = useState(false);
  const [isSuccess, setIsSuccess] = useState(false);
  const { id } = useParams();
  const isNew = id === 'new';
  const [resultBuildings, setResultBuildings] = useState([]);
  const [errors, setErrors] = useState({});
  const [showModal, setShowModal] = useState(false);
  const [savedTemperature, setSavedTemperature] = useState(null);
  const selectedSchedule = useMemo(() =>
    !isNew ? schedules.find((schedule) => schedule.id === id) : {}, [isNew, id, schedules]);
  const [schedule, setSchedule] = useState(isNew ? defaultForm : selectedSchedule && {
    startTime: selectedSchedule.start,
    endTime: selectedSchedule.end,
    title: selectedSchedule.title,
    temperature: temperature === 'F' ? CtoF(selectedSchedule.temperature) : selectedSchedule.temperature
  });
  const [selectedBuildings, setSelectedBuildings] = useState(isNew ? [] : [selectedSchedule && selectedSchedule.building.id]);
  const notFound = !isNew && !selectedSchedule;
  const resetForm = () => {
    if (isNew) {
      setSchedule(defaultForm);
      setSelectedBuildings([]);
    }
  };

  useEffect(() => {
    if (!isNew && selectedSchedule) {
      setSchedule((state) => ({
        ...state,
        temperature: temperature === 'F' ?
          CtoF(selectedSchedule.temperature) :
          selectedSchedule.temperature
      }));
    }
  }, [temperature, selectedSchedule, isNew]);

  const onSubmit = (e) => {
    e.preventDefault();
    setIsSubmitted(true);
    const confBuildings = [];
    const appBuildings = [];
    for (let i = 0; i < selectedBuildings.length; i++) {
      // if the schedule already exists, you have to remove it from the current schedules list.
      let auxSchedules = isNew ? schedules : schedules.filter((schedule) => schedule.id !== id);
      const currentBuilding =
        buildingZones.find((building) => building.id === parseInt(selectedBuildings[i]));
      // This finds a schedule that enters in conflict with the one to be created
      const currentSchedule =
        auxSchedules.find((savedSchedule) => ((savedSchedule.building.id === parseInt(selectedBuildings[i])) &&
          ((savedSchedule.start <= schedule.endTime && savedSchedule.start >= schedule.startTime) ||
            (savedSchedule.end >= schedule.startTime && savedSchedule.end <= schedule.endTime) ||
            (savedSchedule.end >= schedule.endTime && savedSchedule.start <= schedule.startTime))));
      if (currentSchedule) confBuildings.push(currentSchedule.building);
      else appBuildings.push(currentBuilding);
    }
    if (confBuildings.length > 0) {
      setResultBuildings(confBuildings);
    } else {
      const t = temperature === 'F' ? FtoC(schedule.temperature) : schedule.temperature;
      selectedBuildings.forEach((selectedBuilding) => {
        const scheduleData = {
          id: !isNew ? selectedSchedule.id : undefined,
          title: schedule.title || '',
          start: schedule.startTime,
          end: schedule.endTime,
          temperature: t,
          building: buildingZones.find((building) => building.id === parseInt(selectedBuilding))
        };
        isNew ? addSchedule(scheduleData) : updateSchedule(scheduleData);
      })
      setSavedTemperature(t);
      setResultBuildings(appBuildings);
      setIsSuccess(true);
      resetForm();
    }
  };

  const onCancel = () => {
    history.go('/schedules');
  }

  const onCancelModal = () => {
    setShowModal(false);
  }

  const onRemove = () => {
    removeSchedule(selectedSchedule);
    setShowModal(false);
  };

  const showConfirmationModal = () => {
    setShowModal(true);
  };

  const onScheduleChange = (newValue) => {
    setSchedule((state) => ({ ...state, ...newValue }))
  };

  const { isLoading, data: buildingZones } = useQuery('buildings', fetchBuildingZones)

  if (notFound) return (<div className='LoaderContainer'>
    <Alert variant='danger'>
      Schedule doesn't found.
    </Alert>
  </div>)

  if (isLoading) return (<div className='LoaderContainer'>
    <Loader
      type='ThreeDots'
      color='#00BFFF'
      height={100}
      width={100}
      timeout={3000}
    />
  </div>)

  return (<Container className='AppContainer'>
    <h4 className='EditTitle'> {isNew ? 'Add a Schedule for Building Zones' : `Edit Schedule for ${selectedSchedule.building.name}`}</h4>
    <Row>
      <Col lg={4} md={6} xs={12} className='EditCol'>
        <Form onSubmit={onSubmit} className='SchedulesForm'>
          <Form.Group>
            <Form.Label>Title</Form.Label>
            <Form.Control
              type='text'
              placeholder='Add a Description'
              value={schedule.title}
              onChange={({ target: { value } }) => onScheduleChange({ title: value })}
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Start Time</Form.Label>
            <TimePicker
              start='00:00'
              end='23:30'
              step={30}
              value={schedule.startTime}
              onChange={(time) => {
                onScheduleChange({ startTime: time, endTime: time + 1800 });
              }}
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>End Time</Form.Label>
            <TimePicker
              start='00:00'
              end='23:30'
              step={30}
              value={schedule.endTime}
              onChange={(time) => {
                if (time >= schedule.startTime) {
                  onScheduleChange({ endTime: time });
                  setErrors((state) => ({ ...state, endTime: false }));
                } else {
                  setErrors((state) => ({ ...state, endTime: true }));
                }
              }}
            />
            {
              errors.endTime && (<Form.Text className='text-danger'>
                End time must be after start time.
              </Form.Text>)
            }
          </Form.Group>
          {isNew && (<Form.Group>
            <Form.Label>Building Zones</Form.Label>
            <DropdownMultiselect
              options={buildingZones.map((zone) => ({ key: zone.id, label: zone.name }))}
              name='buildings'
              handleOnChange={(selected) => {
                setSelectedBuildings(selected)
              }}
              buttonClass='btn-link'
            />
          </Form.Group>)
          }
          <Form.Group>
            {/** 
           * TODO: this temperature should have a range based on A/C 
           * max and min supported temperatures 
           * **/}
            <Form.Label>
              Temperature {` (°${temperature})`}
            </Form.Label>
            <Form.Control
              type='number'
              value={schedule.temperature}
              onChange={
                ({ target: { value } }) => onScheduleChange({ temperature: value })
              }
            />
          </Form.Group>
          <Button
            variant='success'
            type='submit'
            disabled={selectedBuildings.length === 0}
          >
            Submit
          </Button> {' '}
          <Button variant='warning' onClick={onCancel}>Cancel</Button> {' '}
          {!isNew && <Button variant='danger' onClick={showConfirmationModal}>Delete</Button>}
        </Form>
      </Col>
      <Col className='EditCol'>
        {isSubmitted && !isSuccess && (<Alert variant='danger'>
          Something goes wrong!. We found conflicts with the Building Zones described below.
        </Alert>)}
        {isSubmitted && isSuccess && (<Alert variant='success'>
          Success!. Schedules created succesfully for the Building Zones described below.
        </Alert>)}
        {!isSubmitted && <Alert variant='info'>
          Hello! You will find your scheduling info on this section.
        </Alert>}
        {
          isSubmitted && (<ListGroup>
            <ListGroup.Item className='ScheduleInfoItem'>
              <div>Title</div>
              <div>{schedule.title}</div>
            </ListGroup.Item>
            <ListGroup.Item className='ScheduleInfoItem'>
              <div>Start Time</div>
              <div>{secondsToAMPM(schedule.startTime)}</div>
            </ListGroup.Item>
            <ListGroup.Item className='ScheduleInfoItem'>
              <div>End Time</div>
              <div>{secondsToAMPM(schedule.endTime)}</div>
            </ListGroup.Item>
            <ListGroup.Item>
              <div>{isSuccess ? 'Selected Building Zones' : 'Building Zones with schedule conflicts'}</div>
              {
                resultBuildings.map((building) => (
                  <Badge
                    key={building.id}
                    variant={isSuccess ? 'success' : 'danger'}
                    className='ScheduleBadge'
                    pill
                  >
                    {building.name}
                  </Badge>
                ))
              }
            </ListGroup.Item>
            <ListGroup.Item className='ScheduleInfoItem'>
              <div>Temperature</div>
              <div>
                {temperature === 'F' ? Math.round(CtoF(savedTemperature)) : Math.round(savedTemperature)}
                {` °${temperature}`}
              </div>
            </ListGroup.Item>
          </ListGroup>)
        }
      </Col>
    </Row>
    {
      !isNew && (<Modal
        show={showModal}
        backdrop='static'
        keyboard={false}
        onHide={onCancelModal}
      >
        <Modal.Header closeButton>
          <Modal.Title>Please confirm</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>Are you sure to delete {selectedSchedule.building.name}</p>
        </Modal.Body>
        <Modal.Footer>
          <Button variant='primary' onClick={onCancelModal}>
            Cancel
          </Button>
          <Button variant='danger' onClick={onRemove}>Delete</Button>
        </Modal.Footer>
      </Modal>)
    }
  </Container>)
};

export default EditSchedules;