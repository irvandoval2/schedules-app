import React, { useCallback } from 'react';
import { useUI } from '../../state/useUI';
import BigCalendar from 'react-big-calendar-like-google';
import { useHistory } from 'react-router-dom';
import moment from 'moment';
import 'react-big-calendar-like-google/lib/css/react-big-calendar.css';
import { CtoF, secondsToTime } from '../../utils';


BigCalendar.setLocalizer(BigCalendar.momentLocalizer(moment));

const Schedules = () => {
  const { schedules, temperature, buildingFilter } = useUI();
  const history = useHistory();
  const transformedSchedules = useCallback(() => {
    const filteredSchedules = buildingFilter !== '' 
      ? schedules.filter((schedule) => schedule.building.id === parseInt(buildingFilter)) :
      schedules;
    return filteredSchedules.map((schedule) => {
      const dateStr = moment().format('yyyy-MM-DD');
      return(
      { 
        ...schedule,
        start: moment(`${dateStr} ${secondsToTime(schedule.start)}`).toDate(),
        end: moment(`${dateStr} ${secondsToTime(schedule.end)}`).toDate(),
        title: `${schedule.building.name} -
        (${temperature === 'F' ? Math.round(CtoF(schedule.temperature)) + '°F' : Math.round(schedule.temperature) + '°C'})
        ${schedule.title ? ' - ' + schedule.title : ''}`
      }
    )});
  }, [schedules, temperature, buildingFilter]);

  const onClickEvent = ({ id }) => {
    history.push(`/schedules/${id}`);
  };

  return (
    <BigCalendar
      toolbar={false}
      selectable
      events={transformedSchedules()}
      defaultView='day'
      defaultDate={new Date()}
      onSelectEvent={onClickEvent}
      allDaySlot={false}
    />
  );
}

export default Schedules;
