export const FtoC = (F = 0) => (F - 32) * (5 / 9);

export const CtoF = (C = 0) => (C * (9 / 5)) + 32;

export const secondsToTime = (secs) => {
    const t = new Date(1970, 0, 1);
    t.setSeconds(secs);
    let s = t.toTimeString().substr(0, 5);
    if (secs > 86399)
        s = Math.floor((t - Date.parse('1/1/70')) / 3600000) + s.substr(2);
    return s;
};

export const secondsToAMPM = (seconds) => {
    let time = secondsToTime(seconds);
    time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

    if (time.length > 1) {
        time = time.slice(1);
        time[5] = + time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
        time[0] = +time[0] % 12 || 12; // Adjust hours
    }
    return time.join('');
}